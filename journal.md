MASTER TAL M1.S2	PPE

Creation gitlab
Ajout nouvelle cle ssh

******************************************************************

SEMAINE 2

Correction exercices git :

git merge branch
=> merger la branche courante avec la branche branch

git checkout -b branch
=> création a la tête de la branche "branch" et déplacement dessus (création sans déplacement : git branch branch)

git push --set-upstream origin branch
=> premier push sur la nouvelle branche (déjà créee) "branch"

commande gitk --all
=> visualisation du dépôt git dans lequel la commande est tapée


********************************************************************

SEMAINE 3

def fonction (plop: list[str]) -> List[str]:
=> permet d'indiquer que la fonction prend une liste de string et retourne une liste de string. Au cas où notre code nous donne autre chose, l'editeur nous arrête.
Possibilité d'utiliser mypy (pip install mypy)

réussir à intégrer un programme python dans une pipeline

broken pipe error : dans la boucle où l'on print :
try :
    boucle print
except :
    pass

(nous permet de bypasser la grogne de bash lorsque python est coupé par un autre programme (ex : less, head) avant d'avoir tout écrit).

dans un terminal :
-> g (retourner en haut)
-> G (retourner en haut. shift+g)

module system : import sys :
sys.stdin   => entrée standard          (for line in stdin : je parcours les lignes de mon entrée standard)
sys.stdr    =>
sys.stdout  => sortie standard

sys.argv[0] = nom du script
sys.argv[1] = le premier argument
sys.argv[2] = le deuxieme argument
sys.argv[1:] = tous les arguments


ARGPARSE
pour les scripts compliqués : argparse (bibliothèque standard. aller voir la doc https://docs.org/library/argparse.html):
import argparse

ARGUMENTS :
my_parser = argparse.ArgumentParser()        objet dans lequel on indique les différentes options et arguments
my_parser = argparse.ArgumentParser(description=str)    donne une description du script (doit être une string)

parametrage du parser :
    -> my_parser.add_argument("arg1",help="fails is not given")
    -> my_parser.add_argument(
    "arg2,
    nargs= "?",
    help="An optionnal argument, defaults to None"
)

my_args = my_parser.parse_args()            récupération d'un objet qui contient les arguments de notre script
on peut les appeler : my_args.arg1 ou my_args.arg2 etc

OPTIONS :
-> my parser.add_argument(
    "-n"                    option courte
    "--number-of-times",    option longue
    type=int,               typage
)
-> my_parser.add_argument(
    "--greeting",
    choices=('Hello', 'Hi'),
    help="Plante si ce qu'on donne n'est pas un choix."
)
